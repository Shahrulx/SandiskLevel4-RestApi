﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestApi.Models
{
    public class ModelSubmitTicket
    {
        public string Pic { get; set; }
        public int LineId{get;set;}
        public int MachineId{get;set;}
        public int DowntimeId{get;set;}
        public string TargetGroup{get;set;}
        public string LotNo{get;set;}
        public string CurrentRecepi{get;set;}
    }
}