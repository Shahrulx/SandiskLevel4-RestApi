﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestApi.Models
{


    public class MachineRoot
    {
        public String MachineList { get; set; }
    }

    public class CallStatus
    {
        public String Status { get; set; }
    }

    public class ModelTest
    {
        public string id { get; set; }
    }

    public class ModelDownTime
    {
        public string Id { get; set; }
        public string MachineId { get; set; }
        public string MachineName { get; set; }
        public string Status { get; set; }
        public string LineName { get; set; }
        public string DateTime { get; set; }
        public string StartDate { get; set; }
        public string IsLock { get; set; }
        public string Checklist { get; set; }
        public string DownType { get; set; }

    }


    public class UnlockModel
    {
        public bool Status { get; set; }
        public bool IsAPCChecklistAvailable { get; set; }
        public string Comment { get; set; }
    }

    public class UnlockPostModel
    {
        public string Id { get; set; }
        public string Pic { get; set; }
        public string ToolName { get; set; }
        public string RootCause { get; set; }
        public string Correctiveness { get; set; }
        public string Password { get; set; }
        public string dtstatus { get; set; }
        public string dbIsLock { get; set; }
        public string dbChecklist { get; set; }
    }

}

