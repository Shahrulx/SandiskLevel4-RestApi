﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestApi.Models
{
    public class AssignDownTimeModel
    {
        public class UserInfo
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string UserType { get; set; }

        }

        public class DeviceInfo
        {
            public string DeviceName { get; set; }
            public string Wifi { get; set; }
            public string Username { get; set; }
            public string BatteryLevel { get; set; }
            public string Charging { get; set; }

            public string AppVersion { get; set; }
        }

        public class BarcodeData
        {
            public string Barcode { get; set; }
            public string BoxQty { get; set; }
            public string QtyBox { get; set; }
            public string TotalQty { get; set; }
            public string ExpDate { get; set; }
            public string Pic { get; set; }
        }

        public class ACKDownTime
        {
            public string Id { get; set; }
            public string PIC { get; set; }
            public string Status { get; set; }
            public string Toolname { get; set; }
            public string Comment { get; set; }
            public string Password { get; set; }
            public string CheckList { get; set; }
        }
        public class PassoverDownTime
        {
            public string Id { get; set; }
            public string PIC { get; set; }
            public string PICType { get; set; }
            public string Remarks { get; set; }
        }
    }
}