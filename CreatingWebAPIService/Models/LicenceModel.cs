﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestApi.Models
{
    public class LicenceModel
    {
        public int id { get; set; }
        public string DeviceName { get; set; }
        public string DeviceType { get; set; }
        public string Location { get; set; }
        public string LicKey { get; set; }
        public int LicStatus { get; set; }
        public string DateAdded { get; set; }
        public string AppName { get; set; }
        public string DeviceSerial { get; set; }
        public int Enabled { get; set; }
        public int Trial { get; set; }
    }

    public class LicenceModelFiltred
    {
        public int id { get; set; }
        public string DeviceName { get; set; }
        public string DeviceType { get; set; }
        public string Location { get; set; }
        public int LicStatus { get; set; }
        public string DateAdded { get; set; }
        public string AppName { get; set; }
    }
}