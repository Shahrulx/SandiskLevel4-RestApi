﻿using Newtonsoft.Json;
using RestApi.Controllers;
using RestApi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;
using System.Xml;
using System.Xml.Linq;
using static RestApi.Models.AssignDownTimeModel;

namespace RestApi
{
    public class SQLManager
    {
        static bool SimulationMode = false;
        static bool UseSophic = false;
        static string DebugMode = WebConfigurationManager.AppSettings["DebugMode"];
        static string ProcessId = WebConfigurationManager.AppSettings["ProcessId"];
        static string QueryLog = WebConfigurationManager.AppSettings["QueryLog"];
        static string webservice = WebConfigurationManager.AppSettings["Webservice"];
        public static string connectionString()
        {
            return ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
        }
        public static string connectionString2()
        {
            return ConfigurationManager.ConnectionStrings["dbconnection2"].ConnectionString;
        }

        internal static string GetAppName()
        {
            string data = "";
            SqlConnection conn = new SqlConnection(connectionString());
            try
            {
                var sql = string.Format($"select Value from DeviceRemoteConfig where ProcessId={ProcessId} and Keys='AppName' ");
                var ProcessName = GetAData(conn, sql, "Value");
                return ProcessName;

            }
            catch (Exception ex)
            {
                return "false:" + ex.Message;
            }

            return data;
        }

        internal static string CheckLicence(LicenceModel m)
        {
            LicenceController lc = new LicenceController();
            string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Config.dll";
            int result = lc.GetLocalLicence(m, path);

            if (result == 1)
            {
                //licence checking true go login
                return lc.GetCurrentDeviceConfig(m, path);
            }
            else
            {
                //device not in config check if user want to get licnce from server
                if (m.LicKey.Contains("Server:"))
                {
                    SqlConnection conn = new SqlConnection(connectionString2());
                    var lic = m.LicKey.Replace("Server:", "");
                    string sql = $"select * from Devices where LicKey='{lic}'";
                    var dt = GetDataToDatatable(conn, sql);
                    if (dt.Rows.Count > 0)
                    {
                        LicenceModel lm = dataTableToObject<List<LicenceModel>>(dt)[0];

                        if (lm.DeviceSerial == "-1")
                        {
                            //serial unused. link to this device serial
                            sql = $"Update Devices set DeviceSerial ='{m.DeviceSerial}',LicStatus=1,DateAdded=getdate() where id={lm.id}";
                            AddOrUpdateToDatabase(conn, sql);
                            var timenow = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                            lm.DeviceSerial = m.DeviceSerial;
                            lm.DateAdded = timenow;

                            //add config to current config file in local
                            return lc.UpdateLocalLicence(lm, path);
                        }
                    }
                }
                return lc.FailedJson(m);
            }


            //var stat = lc.CheckLicence(m.DeviceName, m.Location, m.AppName, path);
            //return stat.ToString();
        }

        internal static string LicList()
        {
            LicenceController lc = new LicenceController();

            string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Config.dll";
            var stat = lc.GetRegistredDevice(path);
            return stat.ToString();
        }

        public static string GetAppUpdate(DeviceInfo device)
        {
            string data = "";
            SqlConnection conn = new SqlConnection(connectionString());
            try
            {
                var sql = string.Format($"select id from Devices where ProcessId={ProcessId} and Wifi='{device.Wifi}'");
                var DeviceId = GetAData(conn, sql, "id");

                sql = string.Format($"select Value from DeviceRemoteConfig where ProcessId={ProcessId} and Keys='AppVersion' ");
                var AppVersionFromServer = GetAData(conn, sql, "Value");

                if (DeviceId == null || DeviceId == "" || DeviceId == "0")
                {
                    sql = $"insert into Devices (Wifi,AppVersion,LastUpdate,DeviceType,ProcessId) values ('{device.Wifi}','{device.AppVersion}',getdate(),'Wearable',{ProcessId})";
                    AddOrUpdateToDatabase(conn, sql);
                    //device not registred exit now
                    return "exit";
                }

                if (device.AppVersion != AppVersionFromServer)
                {
                    //device version not same, peform update
                    return "update";
                }
                else
                {
                    //device version same as server, updatedb
                    sql = $"update Devices set AppVersion='{device.AppVersion}', Status='1' where Id='{DeviceId}'";
                    AddOrUpdateToDatabase(conn, sql);
                    return "true";
                }
            }
            catch (Exception ex)
            {
                return "false:" + ex.Message;
            }

        }


        public static string UpdateBarcode(BarcodeData barcodeData)
        {
            SqlConnection conn = new SqlConnection(connectionString());

            string sql = string.Empty;

            //sql = string.Format($"select * from Users where Username='{UserName}' and Password='{Password}' ");
            sql = string.Format($"INSERT INTO [dbo].[Test]([Barcode],[Boxqty] ,[QtyBox],[TotalQty],[ExpDate]) values" +
                $"('{barcodeData.Barcode}','{barcodeData.BoxQty}','{barcodeData.QtyBox}','{barcodeData.TotalQty}','{barcodeData.ExpDate}')");

            try
            {
                AddOrUpdateToDatabase(conn, sql);
                return "true";
            }
            catch (Exception ex)
            {

                return "false" + ex.Message;
            }

        }

        public static string GetUser(string UserName, string Password)
        {
            SqlConnection conn = new SqlConnection(connectionString());

            string sql = string.Empty;
            Password = Encrypt(Password);
            //sql = string.Format($"select * from Users where Username='{UserName}' and Password='{Password}' ");
            sql = string.Format($"select UserType from Users where  ProcessId={ProcessId} and  Username='{UserName}' and Password='{Password}' ");
            DataTable val = GetDataToDatatable(conn, sql);

            if (val.Rows.Count > 0)
                return val.Rows[0]["UserType"].ToString();
            else
                return "0";

        }

        internal static string TestConnection()
        {
            string status = "";

            try
            {
                SqlConnection conn = new SqlConnection(connectionString());
                conn.Open();
                status = "SUCCESS";
                conn.Close();
            }
            catch (Exception ex)
            {
                status = ex.Message;
            }

            return status;
        }

        public static string GetNewDownMachine(string PicType)
        {
            SqlConnection conn = new SqlConnection(connectionString());
            string sql = $"select COUNT(*) as DownCount from vDetailsDownTime where Processid={ProcessId} and status='LOCK' and (PIC is null or PICType like '%{PicType}%')  ";

            var dt1 = GetDataToDatatable(conn, sql);

            var count = dt1.Rows[0]["DownCount"].ToString();
            return count;
            //
        }

        public static string CountLockedMachine(string PIC = "New")
        {
            SqlConnection conn = new SqlConnection(connectionString());
            var _pic = "";
            var usertype = "";

            string t = DateTime.Today.ToString("yyyy-MM-dd");

            string strSQL3 = "";

            strSQL3 = string.Format($"select LineName from Lines where ProcessId={ProcessId} and Status='Active' order by LineName asc");
            var dt0 = GetDataToDatatable(conn, strSQL3);
            dt0.Columns.Add("DownCount", typeof(System.Int32));

            dt0.PrimaryKey = new[] { dt0.Columns["LineName"] };
            foreach (DataRow row in dt0.Rows)
            {
                row["DownCount"] = 0;   // or set it to some other value
            }

            if (PIC.Contains("New"))
            {
                if (PIC.Contains(','))
                {
                    var picarray = PIC.Split(',');
                    _pic = picarray[1];
                    usertype = picarray[2];
                }

                string sql = string.Format($"select Id from Users where  ProcessId={ProcessId} and  Username='{_pic}'");
                var UserID = GetAData(conn, sql, "Id");
                //strSQL3 = $"select count(LineId) as DownCount,LineName from vAssignDetailsDT where PIC is null and PICType='{usertype}' and ACKDate is null group by LineName";
                strSQL3 = $"select count(LineId) as DownCount,LineName from vDetailsDownTime where  ProcessId={ProcessId} and  STATUS='LOCK' AND  PIC is null and (PICType like '%{usertype}%' or PICType is null)  group by LineName";
                //strSQL3 += " AND ( DownTime.PIC='" + UserID + "') OR ( DownTime.PIC IS NULL) and ACKDate IS NULL ";

            }
            else if (PIC.Contains("PCHECKLIST"))
            {
                if (PIC.Contains(','))
                {
                    var picarray = PIC.Split(',');
                    _pic = picarray[1];
                    usertype = picarray[2];
                }

                string sql = string.Format($"select Id from Users where  ProcessId={ProcessId} and  Username='{_pic}'");
                var UserID = GetAData(conn, sql, "Id");
                //strSQL3 = $"select count(LineId) as DownCount,LineName from vAssignDetailsDT where PIC = {UserID} and PICType='{usertype}' and ACKDate is not null group by LineName";

                if (usertype == "admin")
                {
                    strSQL3 = $"select count(LineId) as DownCount,LineName from vDetailsDownTime where  ProcessId={ProcessId} and PIC = {UserID} and STATUS='DONE' group by LineName";

                }
                else
                {
                    strSQL3 = $"select count(LineId) as DownCount,LineName from vDetailsDownTime where  ProcessId={ProcessId} and  PIC = {UserID} and STATUS='DONE'  and PICType like '%{usertype}%'  group by LineName";
                }
            }
            else if (PIC == "Admin" || PIC == "leader")
            {
                strSQL3 += " AND ( DownTime.PIC IS NULL) ";
            }
            else
            {
                if (PIC.Contains(','))
                {
                    var picarray = PIC.Split(',');
                    _pic = picarray[0];
                    usertype = picarray[1];
                }

                string sql = string.Format($"select Id from Users where  ProcessId={ProcessId} and  Username='{_pic}'");
                var UserID = GetAData(conn, sql, "Id");
                //strSQL3 = $"select count(LineId) as DownCount,LineName from vAssignDetailsDT where PIC = {UserID} and PICType='{usertype}' and ACKDate is not null group by LineName";

                if (usertype == "admin")
                {
                    strSQL3 = $"select count(LineId) as DownCount,LineName from vDetailsDownTime where  ProcessId={ProcessId} and  PIC = {UserID} and STATUS='LOCK' group by LineName";

                }
                else
                {
                    strSQL3 = $"select count(LineId) as DownCount,LineName from vDetailsDownTime where  ProcessId={ProcessId} and PIC = {UserID} and STATUS='LOCK'  and PICType like '%{usertype}%'  group by LineName";
                }


            }


            var dt1 = GetDataToDatatable(conn, strSQL3);


            foreach (DataRow row in dt1.Rows)
            {
                DataRow[] rows = dt0.Select("LineName = '" + row["LineName"] + "'");
                int index = dt0.Rows.IndexOf(dt0.Rows.Find(row["LineName"]));
                dt0.Rows[index].SetField("DownCount", row["DownCount"]);

            }

            return DataTableToJSONWithJSONNet(dt0);
        }

        public static string ServiceTest()
        {
            string DummyPageUrl = "http://sophicauto.ddns.net/SanDiskWebService_Validation/SanDiskSdsmEDownTime.asmx/UnlockMachine?" + $"toolName={1}&fromDateTime={1}&toDatetime={1}";

            List<string> dataname = new List<string>() { "Status", "IsAPCChecklistAvailable", "Comment" };
            var _hitpage = HitPage(DummyPageUrl, dataname, false);

            return _hitpage[0].Value.ToString();
        }

        public static string GetDownMachineByLine(string LineNameAndPic)
        {
            SqlConnection conn = new SqlConnection(connectionString());

            var val = LineNameAndPic.Split(',');
            //val[0], val[1]
            var LineName = val[0];
            var PIC = val[1];
            var PICType = val[2];
            string sql = "";


            if (PIC == "")
                sql = $"select Id,MachineId,MachineName,Status,LineName,DateTime,StartDate,IsLock,Checklist,EndTime,Description,DownType,Color,LotId from vDetailsDownTime where  ProcessId={ProcessId} and Status='LOCK' and PIC IS NULL and PICType like '%{PICType}%' and LineName='{LineName}'";
            else if (val.Contains("PCHECKLIST"))
            {
                sql = string.Format($"select Id from Users where ProcessId={ProcessId} and  Username='{PIC}'");
                var UserID = GetAData(conn, sql, "Id");
                sql = $"select Id,MachineId,MachineName,Status,LineName,DateTime,StartDate,IsLock,Checklist,EndTime,Description,DownType,Color,LotId from vDetailsDownTime where  ProcessId={ProcessId} and Status='DONE' and PIC='{UserID}' and LineName='{LineName}' ";

            }
            else
            {
                sql = string.Format($"select Id from Users where ProcessId={ProcessId} and  Username='{PIC}'");
                var UserID = GetAData(conn, sql, "Id");
                sql = $"select Id,MachineId,MachineName,Status,LineName,DateTime,StartDate,IsLock,Checklist,EndTime,Description,DownType,Color,LotId from vDetailsDownTime where  ProcessId={ProcessId} and Status='LOCK' and PIC='{UserID}' and LineName='{LineName}' ";
            }

            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));
        }

        internal static string UpdateBattery(DeviceInfo device)
        {
            string data = "";
            SqlConnection conn = new SqlConnection(connectionString());
            try
            {
                var sql = string.Format($"select Id from Devices where  ProcessId={ProcessId} and  Wifi='{device.Wifi}'");
                var DeviceID = GetAData(conn, sql, "Id");

                if (device.Username == "")
                {
                    sql = $"insert into DeviceHealth (DeviceId,CurrentBatery) values ('{DeviceID}','{device.BatteryLevel}')";
                }
                else
                {
                    var sql2 = string.Format($"select Id from Users where ProcessId={ProcessId} and  Username='{device.Username}'");
                    var UserId = GetAData(conn, sql2, "Id");

                    sql = $"insert into DeviceHealth (DeviceId,CurrentBatery,UserId) values ('{DeviceID}','{device.BatteryLevel}','{UserId}')";
                }

                if (device.Charging == "0")
                {
                    //only update to device healt if not charhing
                    AddOrUpdateToDatabase(conn, sql);
                }


                if (device.Charging == "1")
                {
                    if (device.Username == "")
                    {
                        sql = $"update Devices set UserId=NULL, BatteryLevel='{device.BatteryLevel}', Status='1' where  ProcessId={ProcessId} and  Id='{DeviceID}'";
                    }
                    else
                    {
                        var sql2 = string.Format($"select Id from Users where  ProcessId={ProcessId} and Username='{device.Username}'");
                        var UserId = GetAData(conn, sql2, "Id");

                        sql = $"update Devices set UserId='{UserId}', BatteryLevel='{device.BatteryLevel}', Status='1' where  ProcessId={ProcessId} and  Id='{DeviceID}'";
                    }
                }
                else
                {
                    if (device.Username == "")
                    {
                        sql = $"update Devices set UserId=NULL, BatteryLevel='{device.BatteryLevel}', Status='0' where ProcessId={ProcessId} and  Id='{DeviceID}'";
                    }
                    else
                    {
                        var sql2 = string.Format($"select Id from Users where  ProcessId={ProcessId} and Username='{device.Username}'");
                        var UserId = GetAData(conn, sql2, "Id");

                        sql = $"update Devices set UserId='{UserId}', BatteryLevel='{device.BatteryLevel}', Status='0' where  ProcessId={ProcessId} and  Id='{DeviceID}'";
                    }
                }

                AddOrUpdateToDatabase(conn, sql);
            }
            catch (Exception ex)
            {
                return "false:" + ex.Message;
            }
            return data;
        }

        public static string ACKDownTime(ACKDownTime p)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionString());

                //get pic id based on name
                string sql = string.Format($"select Id from Users where  ProcessId={ProcessId} and  Username='{p.PIC}'");
                var UserID = GetAData(conn, sql, "Id");

                WriteToLog(sql);

                //get startdate and islock
                sql = string.Format($"select StartDate,IsLock,Remarks,PICType,Alarm,MachineId from vDetailsDownTime where  ProcessId={ProcessId} and  Id={p.Id}");
                WriteToLog(sql);
                DataTable dt = GetDataToDatatable(conn, sql);

                DateTime sdt = Convert.ToDateTime(dt.Rows[0][0]);
                var startDate = sdt.ToString("yyyy-MM-dd HH:mm:ss");

                //var startDate = dt.Rows[0][0].ToString();
                bool isLock = bool.Parse(dt.Rows[0][1].ToString());
                string Remarks = dt.Rows[0][2].ToString();
                string PICType = dt.Rows[0][3].ToString();
                string Alarm = dt.Rows[0][4].ToString();
                p.Toolname = dt.Rows[0][5].ToString();

                string unlockStatus = "";

                //get machine name
                sql = $"SELECT MachineName FROM Machines where  ProcessId={ProcessId} and id = {p.Toolname}";
                p.Toolname = GetAData(conn, sql, "MachineName");


                if (SimulationMode)
                {
                    //if (p.Comment == "false")
                    //{
                    //    unlockStatus = "false";
                    //}
                    //else
                    //{
                    sql = $"update DTAttend  set NextPIC='{UserID}' where  ProcessId={ProcessId} and  DTId = '{p.Id}' AND NextPIC IS NULL AND ENDDate IS NOT NULL ";
                    AddOrUpdateToDatabase(conn, sql);

                    sql = string.Format("insert into DTAttend (AssignDate, ACKDate, DTId, PIC,ProcessId) values ('{0}',CURRENT_TIMESTAMP,'{1}','{2}',{3})", startDate, p.Id, UserID, ProcessId);
                    AddOrUpdateToDatabase(conn, sql);

                    sql = $"UPDATE DownTime SET PIC='{UserID}', Status='{ p.Status}',Checklist='{p.CheckList}' where  ProcessId={ProcessId} and Id='{p.Id}'";
                    AddOrUpdateToDatabase(conn, sql);
                    unlockStatus = "true";
                    //}
                    unlockStatus = "true";

                }
                else
                {

                    if (isLock == true)
                    {

                        if (PICType == "EE")
                        {

                            string DummyPageUrl = "";
                            List<string> dataname = new List<string>() { "Status", "IsAPCChecklistAvailable", "Comment" };
                            List<XElement> _hitpage;

                           if (UseSophic)
                            {
                                DummyPageUrl = "http://sophicauto.ddns.net/SanDiskWebService_Validation/SanDiskSdsmEDownTime.asmx/UnlockEquipmentForAll?toolName=" + p.Toolname + "&username=" + p.PIC + "&password=" + p.Password + "&comment=" + p.Comment + "&solution=true&rtiEvent=" + Remarks + "";
                            }

                            else
                            {

                                DummyPageUrl = $"{webservice}/UnlockEquipmentForAll?toolName=" + p.Toolname + "&username=" + p.PIC + "&password=" + p.Password + "&comment=AttendMachine&solution=true&rtiEvent=" + Remarks + "&alarmId=" + Alarm + "&source=DEVICE";
                                _hitpage = HitPage(DummyPageUrl, dataname);
                                unlockStatus = _hitpage[0].Value.ToString().ToLower();

                                if (unlockStatus.ToLower() == "false")
                                {
                                    WriteToLog("Stat:" + unlockStatus + "UnlockUrl1" + DummyPageUrl);
                                    WriteToLog(_hitpage[2].Value.ToString());

                                    Alarm = "RW000001";
                                    DummyPageUrl = $"{webservice}/UnlockEquipmentForAll?toolName=" + p.Toolname + "&username=" + p.PIC + "&password=" + p.Password + "&comment=AttendMachine&solution=true&rtiEvent=" + Remarks + "&alarmId=" + Alarm + "&source=DEVICE";
                                    _hitpage = HitPage(DummyPageUrl, dataname);
                                    unlockStatus = _hitpage[0].Value.ToString().ToLower();
                                }

                            }

                            if (unlockStatus.ToLower() == "false")
                            {
                                WriteToLog("Stat:" + unlockStatus + "UnlockUrl2" + DummyPageUrl);
                                DummyPageUrl = $"{webservice}/UnlockEquipment?toolName=" + p.Toolname + "&username=" + p.PIC + "&password=" + p.Password + "&comment=AttendMachine&solution=true&rtiEvent=" + Remarks + "";

                                dataname = new List<string>() { "Status", "IsAPCChecklistAvailable", "Comment" };
                                _hitpage = HitPage(DummyPageUrl, dataname);

                                unlockStatus = _hitpage[0].Value.ToString();
                                WriteToLog("Stat:" + unlockStatus + "UnlockUrl3" + DummyPageUrl);
                            }

                        }
                        else
                        {
                            unlockStatus = "true";
                        }


                    }
                    else
                    {
                        unlockStatus = "true";
                    }


                    if (unlockStatus == "true")
                    {
                        sql = $"update DTAttend  set NextPIC='{UserID}' where  ProcessId={ProcessId} and  DTId = '{p.Id}' AND NextPIC IS NULL AND ENDDate IS NOT NULL ";
                        AddOrUpdateToDatabase(conn, sql);

                        sql = string.Format("insert into DTAttend (AssignDate, ACKDate, DTId, PIC,ProcessId) values ('{0}',CURRENT_TIMESTAMP,'{1}','{2}',{3})", startDate, p.Id, UserID, ProcessId);
                        AddOrUpdateToDatabase(conn, sql);

                        sql = $"UPDATE DownTime SET PIC='{UserID}', Status='{ p.Status}',Checklist='{p.CheckList}' where  ProcessId={ProcessId} and Id='{p.Id}'";
                        AddOrUpdateToDatabase(conn, sql);

                    }
                    else
                    {
                        unlockStatus = "false:";
                    }
                }
                return unlockStatus;
            }
            catch (Exception ex)
            {
                return "false:" + ex.Message;
            }
        }

        public static string PassOver(PassoverDownTime p)
        {
            string dbIsLock = "";
            string MachineName = "";
            string dbErrorRemarks = "";
            string DummyPageUrl = "";
            string dtPICType = "";
            string dbDTType = "";
            string ACKDate = "";
            string dtnow = "";
            string dbChecklist = "";
            string GotChecklist = "";

            string result = "false";
            try
            {
                string Group = p.PICType;
                string Remark = p.Remarks;

                SqlConnection conn = new SqlConnection(connectionString());

                string strSQL4 = "SELECT TOP 1 * FROM vDetailsDownTime  Where Id ='" + p.Id + "'";
                DataTable dt = GetDataToDatatable(conn, strSQL4);

                for (int i = 0; i < 1; i++)
                {
                    dbIsLock = dt.Rows[i]["IsLock"].ToString();
                    MachineName = dt.Rows[i]["MachineName"].ToString();
                    dbErrorRemarks = dt.Rows[i]["Remark"].ToString();
                    dtPICType = dt.Rows[i]["PICType"].ToString();
                    dbDTType = dt.Rows[i]["DownType"].ToString();
                    dbChecklist = dt.Rows[i]["Checklist"].ToString();
                    GotChecklist = dt.Rows[i]["GotChecklist"].ToString();
                }

                if (dbChecklist != "-")
                {
                    GotChecklist = "1";
                }

                if (dtPICType == "EE" && dbIsLock.ToLower() == "true")
                {
                    string chk = HitPagelock(MachineName, p.PIC, dbIsLock.ToLower());
                    WriteToLog("PassOver Stat:" + chk);
                    if (chk == "pass")
                    {
                        svdatapassover(conn, p.Id, Group, Remark);
                        result = "true";


                    }
                    else
                    {
                        result = "false";
                    }
                }
                else
                {
                    svdatapassover(conn, p.Id, Group, Remark);
                    result = "true";

                }
                WriteToLog("PassOver Stat:" + result);
                return result;
            }
            catch (Exception ex)
            {
                return "false:" + ex.Message;
            }
        }

        public static void svdatapassover(SqlConnection conn, string Id, string Group, string Remark)
        {
            string sql = $"Update DownTime Set PIC=Null , PICType='{Group}'  where  ProcessId={ProcessId} and Id='" + Id + "'";
            AddOrUpdateToDatabase(conn, sql);
            sql = $"Update DTAttend Set ENDDate=getdate()  , Remarks='{Remark}' where  ProcessId={ProcessId} and DTId='" + Id + "' AND ENDDate is null";
            AddOrUpdateToDatabase(conn, sql);

        }

        public static string HitPagelock(string MachineName, string Username, string dbIsLock)
        {
            if (dbIsLock == "false" || SimulationMode == true)
            {
                return "true";
            }

            string DummyPageUrl = $"{webservice}/LockEquipment?toolName=" + MachineName + "&username=" + Username + "";

            var urlstring = DummyPageUrl;
            string result = "";
            string result2 = "";
            string check = "";

            // Retrieve XML document  
            XmlTextReader reader = new XmlTextReader(urlstring);

            // Skip non-significant whitespace  
            reader.WhitespaceHandling = WhitespaceHandling.Significant;

            // Read nodes one at a time  
            while (reader.Read())
            {
                string chk = reader.NodeType.ToString();
                // Print out info on node  
                if (chk == "Text")
                {
                    result += reader.Value.ToString();
                }
                // result2 +=  "nodetype:"+reader.NodeType.ToString() +"name:"+reader.Name+",data: "+ reader.Value;

            }

            string data = result;

            if (data == "truefalse" || data == "truetrue")
            {


                check = "true";

            }
            else
            {
                check = "false";

            }

            return check;

        }

        public static string checkCheclist(string MachineName, string DownType, string dbErrorRemarks, string ACKDate, string GotChecklist)
        {

            if (GotChecklist == "0" || SimulationMode == true)
                return "true";

            string dtnow = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
            string DummyPageUrl = "";
            string result = "";


            if (UseSophic)
            {

                DummyPageUrl = "http://sophicauto.ddns.net/SanDiskWebService_Validation/SanDiskSdsmEDownTime.asmx/IsEformSubmittedForWB?" + $"toolName={MachineName}&fromDateTime={ACKDate}&toDatetime={dtnow}";
            }

            else if (DownType == "APC")
            {
                DummyPageUrl = $"{webservice}/IsAPCChecklistSubmitted?toolName=" + MachineName + "&rtiEvent=" + dbErrorRemarks + "&fromDateTime=" + ACKDate + "&toDatetime=" + dtnow + "";

            }
            else if (DownType == "e-form")
            {
                DummyPageUrl = $"{webservice}/IsEformSubmittedForWB?toolName=" + MachineName + "&fromDateTime=" + ACKDate + "&toDatetime=" + dtnow + "";
            }

            List<string> dataname = new List<string>() { "Status" };
            var _hitpage = HitPage(DummyPageUrl, dataname, true);

            string unlockStatus = _hitpage[0].Value.ToString();

            if (unlockStatus.ToLower() == "true")
            {
                result = unlockStatus;
            }
            else
            {
                result = unlockStatus;
            }

            return result;
        }

        public static string ACKDownTimeCheckCanAttend(string Id)
        {
            SqlConnection conn = new SqlConnection(connectionString());
            string sql = string.Format($"select pic from DownTime where  ProcessId={ProcessId} and id='{0}'", Id);
            string checkPIC = GetAData(conn, sql, "PIC");

            if (checkPIC != "0")
                return "false";

            return "true";
        }

        public static string GetLine()
        {
            SqlConnection conn = new SqlConnection(connectionString());
            string sql = string.Empty;
            //sql = string.Format("select LineName from Lines where Status='Active'");
            sql = string.Format("select Id from Lines");
            WriteToLog(sql);
            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));
        }

        public static string GetAllLine()
        {
            SqlConnection conn = new SqlConnection(connectionString());
            string sql = string.Empty;
            sql = $"select Id,LineName from Lines where ProcessId={ProcessId} and Status='ACTIVE' ";
            WriteToLog(sql);
            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));
        }

        public static string GetMachineByLineName(string LineName)
        {
            SqlConnection conn = new SqlConnection(connectionString());
            var sql = $"select id from Lines where LineName='{LineName}' and ProcessId={ProcessId}";
            var id = GetAData(conn, sql, "id");
            sql = $"select * from Machines where LineId={id} and ProcessId={ProcessId}";
            WriteToLog(sql);
            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));
        }

        public static string GetDownTimeDescription()
        {
            SqlConnection conn = new SqlConnection(connectionString());
            var sql = $"SELECT * FROM ErrorCode where ProcessId={ProcessId}";
            WriteToLog(sql);
            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));
        }

        public static string SubmitTicket(ModelSubmitTicket m)
        {
            SqlConnection conn = new SqlConnection(connectionString());

            ///check if machine already down
            var sql = "SELECT Count(*) as Count from DownTime where MachineId = '" + m.MachineId + "' and EndTime Is null";
            var stat = GetAData(conn, sql, "Count");
            if (stat == "1")
            {
                return "Error:Machine Already Down";
            }
            sql = "SELECT MachineName from Machines where Id = '" + m.MachineId + "' ";
            var MachineName = GetAData(conn, sql, "MachineName");

            sql = "Select IsLock from ErrorCode where Id=" + m.DowntimeId;
            var IsLock = GetAData(conn, sql, "IsLock");
            var result = insertData(conn, m);
            return "Success";

            //if (IsLock.ToLower() == "true")
            //{
            //    //peform lock
            //    List<string> dataname = new List<string>() { "Status", "IsAPCChecklistAvailable" };
            //    var _hitpage = HitPagelock(MachineName,m.Pic, IsLock);
            //    if (_hitpage=="true")
            //    {
            //        var result = insertData(conn, m);
            //        return "Success";
            //        //return result;
            //    }
            //    else
            //    {
            //        return "Error: Lock Machine.Wrong Username?";

            //    }
            //}
            //else
            //{
            //    //just insertdata
            //    var result = insertData(conn, m);
            //    //return result;
            //    return "Success";
            //}


        }

        static string insertData(SqlConnection conn, ModelSubmitTicket m)
        {
            string sql = string.Format($"select Id from Users where ProcessId={ProcessId} and Username='{m.Pic}'");
            var userId = GetAData(conn, sql, "Id");

            var strSQL4 = "  INSERT INTO DownTime (LineId,MachineId,ErrorCodeId,Status, PIC, PICType,StartDate, CreatedBy, LotId,CurrentRecipe,NextRecipe,ProcessId) OUTPUT Inserted.ID VALUES ";
            strSQL4 += "   ('" + m.LineId + "'";
            strSQL4 += "  , '" + m.MachineId + "'";
            strSQL4 += "  , '" + m.DowntimeId + "'";
            strSQL4 += "  , 'LOCK'";
            strSQL4 += "  , null";
            strSQL4 += "  , '" + m.TargetGroup + "'";
            strSQL4 += " ,getdate(), '" + userId + "', '" + m.LotNo + "'";
            strSQL4 += " ,'" + m.CurrentRecepi + "'";
            strSQL4 += " ,'" + "-" + "'," + ProcessId + ")";

            var id = AddAndReturn(conn, strSQL4);

            string auditSql = "insert into AuditTrail (UserId, TableChanges, Changes) values ('" + userId + "','DownTime','Add " + id + "')";
            //WriteToLog(sql);
            return id.ToString();

        }

        public static string UnlockMachine(UnlockPostModel unlockPostModel)
        {
            SqlConnection conn = new SqlConnection(connectionString());

            var result = "";

            string Username = unlockPostModel.Pic;
            string DummyPageUrl = "";
            string dbIsLock = "";
            string MachineName = "";
            string dbErrorRemarks = "";

            string dtPICType = "";
            string dbDTType = "";
            string ACKDate = "";
            string dtnow = "";
            string dbChecklist = "";
            string GotChecklist = "";

            //unlockPostModel.Pic = "30948";
            //unlockPostModel.Password = "13Dem13Mech@";
            UnlockModel unlockModel = new UnlockModel();

            if (unlockPostModel.RootCause.Contains("test,"))
            {
                var _data = unlockPostModel.RootCause.Split(',');
                if (_data[1] == "apcdone")
                {
                    insertdata(unlockPostModel.Id, unlockPostModel.Pic, "UNLOCK", unlockPostModel.dbChecklist);
                }
                else if (_data[1] == "gotapc")
                {
                    insertdata(unlockPostModel.Id, unlockPostModel.Pic, unlockPostModel.dtstatus, "test");
                }
                result = "true";
            }

            else
            {
                string strSQL4 = $"SELECT TOP 1 * FROM vDetailsDownTime  Where ProcessId={ProcessId} and Id ='" + unlockPostModel.Id + "'";
                var ds0 = GetDataToDatatable(conn, strSQL4);
                for (int i = 0; i < 1; i++)
                {
                    dbIsLock = ds0.Rows[i]["IsLock"].ToString();
                    MachineName = ds0.Rows[i]["MachineName"].ToString();
                    dbErrorRemarks = ds0.Rows[i]["Remark"].ToString();
                    dtPICType = ds0.Rows[i]["PICType"].ToString();
                    dbDTType = ds0.Rows[i]["DownType"].ToString();
                    dbChecklist = ds0.Rows[i]["Checklist"].ToString();
                    GotChecklist = ds0.Rows[i]["GotChecklist"].ToString();
                }



                var chklst = "true";
                var updatestat = "DONE";
                if (dbChecklist == "-")
                    updatestat = "CLOSE";

                if (chklst == "true")
                {
                    string sql = string.Format($"select Id from Users where ProcessId={ProcessId} and Username='{unlockPostModel.Pic}'");
                    var UserID = GetAData(conn, sql, "Id");
                    sql = string.Format($"select Remarks from vDetailsDownTime where ProcessId={ProcessId} and id='{0}'", unlockPostModel.Id);
                    string Remarks = GetAData(conn, sql, "Remarks");

                    sql = $" Update DownTime Set Status='{updatestat}' , EndTime=getdate(),Remarks='{unlockPostModel.RootCause}' , PIC='" + UserID + $"' where  ProcessId={ProcessId} and Id='" + unlockPostModel.Id + "'";
                    AddOrUpdateToDatabase(conn, sql);
                    sql = " Update DTAttend Set ENDDate=getdate()  , Remarks='" + unlockPostModel.Correctiveness + $"' where  ProcessId={ProcessId} and DTId='" + unlockPostModel.Id + "' AND ENDDate is null";
                    AddOrUpdateToDatabase(conn, sql);

                    result = "true";

                }
                else
                {
                    result = "false:";
                }
            }

            return result;
        }

        public static string CloseTicket(UnlockPostModel unlockPostModel)
        {
            SqlConnection conn = new SqlConnection(connectionString());

            var result = "";

            string Username = unlockPostModel.Pic;
            string DummyPageUrl = "";
            string dbIsLock = "";
            string MachineName = "";
            string dbErrorRemarks = "";

            string dtPICType = "";
            string dbDTType = "";
            string ACKDate = "";
            string dtnow = "";
            string dbChecklist = "";
            string GotChecklist = "";

            UnlockModel unlockModel = new UnlockModel();

            string strSQL4 = $"SELECT TOP 1 * FROM vDetailsDownTime  Where  ProcessId={ProcessId} and  Id ='" + unlockPostModel.Id + "'";
            var ds0 = GetDataToDatatable(conn, strSQL4);
            for (int i = 0; i < 1; i++)
            {
                dbIsLock = ds0.Rows[i]["IsLock"].ToString();
                MachineName = ds0.Rows[i]["MachineName"].ToString();
                dbErrorRemarks = ds0.Rows[i]["Remark"].ToString();
                dtPICType = ds0.Rows[i]["PICType"].ToString();
                dbDTType = ds0.Rows[i]["DownType"].ToString();
                dbChecklist = ds0.Rows[i]["Checklist"].ToString();
                GotChecklist = ds0.Rows[i]["GotChecklist"].ToString();
            }
            string sql = string.Format($"select Id from Users where  ProcessId={ProcessId} and Username='{unlockPostModel.Pic}'");
            var UserID = GetAData(conn, sql, "Id");

            sql = $" Update DownTime Set Status='CLOSE' , EndTime=getdate(),Remarks='{unlockPostModel.RootCause}' , PIC='" + UserID + $"' where  ProcessId={ProcessId} and  Id='" + unlockPostModel.Id + "'";
            try
            {
                AddOrUpdateToDatabase(conn, sql);
                result = "true";
            }
            catch (Exception ex)
            {
                result = "false:" + ex.Message;

            }





            return result;
        }

        public static string CloseMachine(UnlockPostModel unlockPostModel)
        {
            SqlConnection conn = new SqlConnection(connectionString());

            var result = "";

            string Username = unlockPostModel.Pic;
            string DummyPageUrl = "";
            string dbIsLock = "";
            string MachineName = "";
            string dbErrorRemarks = "";

            string dtPICType = "";
            string dbDTType = "";
            string ACKDate = "";
            string dtnow = "";
            string dbChecklist = "";
            string GotChecklist = "";

            //unlockPostModel.Pic = "30948";
            //unlockPostModel.Password = "13Dem13Mech@";
            UnlockModel unlockModel = new UnlockModel();


            string strSQL4 = $"SELECT TOP 1 * FROM vDetailsDownTime  Where  ProcessId={ProcessId} and  Id ='" + unlockPostModel.Id + "'";
            var ds0 = GetDataToDatatable(conn, strSQL4);
            for (int i = 0; i < 1; i++)
            {
                dbIsLock = ds0.Rows[i]["IsLock"].ToString();
                MachineName = ds0.Rows[i]["MachineName"].ToString();
                dbErrorRemarks = ds0.Rows[i]["Remark"].ToString();
                dtPICType = ds0.Rows[i]["PICType"].ToString();
                dbDTType = ds0.Rows[i]["DownType"].ToString();
                dbChecklist = ds0.Rows[i]["Checklist"].ToString();
                GotChecklist = ds0.Rows[i]["GotChecklist"].ToString();
            }

            if (dbChecklist != "-")
            {
                GotChecklist = "1";
            }

            string strSQL3 = $"SELECT TOP 1 AssignDate as ACKDate FROM[vDetailsDownTime]  Where  ProcessId={ProcessId} and  Id = { unlockPostModel.Id }";

            ACKDate = GetAData(conn, strSQL3, "ACKDate");
            DateTime sdt = Convert.ToDateTime(ACKDate);
            ACKDate = sdt.ToString("yyyy-MM-dd HH:mm");

            string chklst = checkCheclist(MachineName, dbChecklist, dbErrorRemarks, ACKDate, GotChecklist);
            if (chklst == "true")
            {
                string sql = string.Format($"select Id from Users where  ProcessId={ProcessId} and  Username='{unlockPostModel.Pic}'");
                var UserID = GetAData(conn, sql, "Id");
                sql = string.Format($"select Remarks from vDetailsDownTime where ProcessId={ProcessId} and  id='{0}'", unlockPostModel.Id);
                string Remarks = GetAData(conn, sql, "Remarks");


                sql = $" Update DownTime Set Status='CLOSE' where ProcessId={ProcessId} and  Id='" + unlockPostModel.Id + "'";
                AddOrUpdateToDatabase(conn, sql);


                result = "true";

            }
            else
            {
                result = "false:";
            }


            return result;
        }


        public static string UpdateBeacon(string positions)
        {
            SqlConnection conn = new SqlConnection(connectionString());
            string sql = "";

            try
            {
                var _positions = positions.Split(',');
                sql = $"  insert into Beacon(xpos,ypos,startdate,device) values ('{_positions[0]}','{_positions[1]}',getdate(),'{_positions[2]}')";
                AddOrUpdateToDatabase(conn, sql);
                return "true";
            }
            catch (Exception ex)
            {

                return "false:" + ex.Message;
            }



        }
        private static void WriteToLog(string val)
        {
            if (DebugMode == "false")
            {
                return;
            }
            // Create a file to write to.
            string read = "";
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Log/debug.txt";
                string createText = val + Environment.NewLine;
                File.AppendAllText(path, createText);
            }
            catch (Exception ex)
            {
                read = ex.Message;
            }
        }



        #region sql and json

        public static string Reset()
        {
            String stat = "";

            try
            {
                stat = "true";
            }
            catch (Exception ex)
            {
                stat = "false:";
            }

            return stat;
        }

        public static string DataTableToJSONWithJSONNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(table);
            return JSONString;
        }

        public static bool AddOrUpdateToDatabase(SqlConnection conn, string sqlString)
        {
            bool isRemove = false;

            if (QueryLog == "1")
            {
                WriteToLog(sqlString);
            }

            try
            {
                conn.Open();

                SqlCommand com = new SqlCommand(sqlString, conn);
                com.ExecuteNonQuery();
                isRemove = true;
            }
            catch (SqlException e)
            {
                if (e.Message.ToString().Contains("duplicate"))
                {
                    isRemove = true;
                }
                else
                {
                    throw e;
                }
                isRemove = false;
            }
            finally
            {
                conn.Close();
            }

            return isRemove;
        }

        public static int AddAndReturn(SqlConnection conn, string sqlString)
        {
            bool isRemove = false;
            int id = -1;

            if (QueryLog == "1")
            {
                WriteToLog(sqlString);
            }
            try
            {
                conn.Open();

                SqlCommand com = new SqlCommand(sqlString, conn);
                //com.ExecuteNonQuery();
                id = int.Parse(com.ExecuteScalar().ToString());
                isRemove = true;
            }
            catch (SqlException e)
            {
                if (e.Message.ToString().Contains("duplicate"))
                {
                    isRemove = true;
                }
                else
                {
                    throw e;
                }
                isRemove = false;
            }
            finally
            {
                conn.Close();
            }

            return id;
        }

        public static string DataTableToJsonObj(DataTable dt)
        {
            DataSet ds = new DataSet();
            ds.Merge(dt);
            StringBuilder JsonString = new StringBuilder();
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                JsonString.Append("[");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    JsonString.Append("{");
                    for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                    {
                        if (j < ds.Tables[0].Columns.Count - 1)
                        {
                            JsonString.Append("\"" + ds.Tables[0].Columns[j].ColumnName.ToString() + "\":" + "\"" + ds.Tables[0].Rows[i][j].ToString() + "\",");
                        }
                        else if (j == ds.Tables[0].Columns.Count - 1)
                        {
                            JsonString.Append("\"" + ds.Tables[0].Columns[j].ColumnName.ToString() + "\":" + "\"" + ds.Tables[0].Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == ds.Tables[0].Rows.Count - 1)
                    {
                        JsonString.Append("}");
                    }
                    else
                    {
                        JsonString.Append("},");
                    }
                }
                JsonString.Append("]");
                return JsonString.ToString();
            }
            else
            {
                return null;
            }
        }

        public static DataTable GetDataToDatatable(SqlConnection conn, string query)
        {
            DataTable dt = new DataTable();
            SqlDataReader dr = null;
            if (QueryLog == "1")
            {
                WriteToLog(query);
            }

            try
            {
                conn.Open();

                SqlCommand mCmd = new SqlCommand(query, conn);
                mCmd.ExecuteNonQuery();
                dr = mCmd.ExecuteReader();
                dt.Load(dr);

            }
            catch (SqlException ex)
            {
                var tt = ex;
            }
            finally
            {
                if (dr != null)
                    if (!dr.IsClosed)
                        dr.Close();

                conn.Close();
            }

            return dt;
        }

        public static string GetAData(SqlConnection conn, string sqlString, string columName) //function to return a value
        {
            SqlDataReader dr = null;
            string value = "0";

            if (QueryLog == "1")
            {
                WriteToLog(sqlString);
            }

            try
            {
                conn.Open();

                SqlCommand mCmd = new SqlCommand(sqlString, conn);

                mCmd.ExecuteNonQuery();
                dr = mCmd.ExecuteReader();

                if (dr.Read())
                {
                    if (dr[columName].ToString().Length > 0)
                        value = dr[columName].ToString();
                }
            }
            catch (SqlException e)
            {

            }
            finally
            {
                if (dr != null)
                    if (!dr.IsClosed)
                        dr.Close();

                conn.Close();
            }

            return value;
        }

        private static string Encrypt(string Password)
        {
            //create new instance of md5
            MD5 md5 = MD5.Create();

            //convert the input text to array of bytes
            byte[] hashData = md5.ComputeHash(Encoding.Default.GetBytes(Password));

            //create new instance of StringBuilder to save hashed data
            StringBuilder returnValue = new StringBuilder();

            //loop for each byte and add it to StringBuilder
            for (int i = 0; i < hashData.Length; i++)
            {
                returnValue.Append(hashData[i].ToString());
            }

            // return hexadecimal string
            string hashPassword = returnValue.ToString();

            return hashPassword;
        }

        #endregion

        private static void insertdata(string Id, string PIC, string dtstatus, string dbChecklist)
        {
            SqlConnection conn = new SqlConnection(connectionString());

            if (dtstatus == "LOCK" && dbChecklist == "-")
            {
                string delquery = " Update DownTime Set EndTime=getdate()  , Status='DONE', PIC='" + PIC + $"' where  ProcessId={ProcessId} and  Id='" + Id + "'";
                AddOrUpdateToDatabase(conn, delquery);

                delquery = $" Update DTAttend Set ENDDate=getdate() where  ProcessId={ProcessId} and  DTId='" + Id + "' AND ENDDate is null";
                AddOrUpdateToDatabase(conn, delquery);

            }
            else if (dtstatus == "LOCK" && dbChecklist != "-")
            {
                string delquery = " Update DownTime Set EndTime=getdate()  , Status='UNLOCK', PIC='" + PIC + "' where Id='" + Id + "'";
                AddOrUpdateToDatabase(conn, delquery);

                delquery = $" Update DTAttend Set ENDDate=getdate() where  ProcessId={ProcessId} and  DTId='" + Id + "' AND ENDDate is null";
                AddOrUpdateToDatabase(conn, delquery);
            }
            else if (dtstatus == "UNLOCK")
            {
                string delquery = " Update DownTime Set Status='DONE', PIC='" + PIC + $"' where  ProcessId={ProcessId} and  Id='" + Id + "'";
                AddOrUpdateToDatabase(conn, delquery);

            }

        }


        private static List<XElement> HitPage(string DummyPageUrl, List<string> dataname, bool NoChild = false)
        {
            List<XElement> list = new List<XElement>();

            XDocument doc = XDocument.Load(DummyPageUrl);
            foreach (var item in dataname)
            {

                if (NoChild)
                {
                    XElement x = XElement.Parse(doc.ToString());
                    var val = x.Value.ToString();
                    XElement x2 = XElement.Parse($"<Root><Child>{val}</Child></Root>");
                    list.Add(x2);
                }
                else
                {
                    var data = doc.Descendants(XNamespace.Get("http://tempuri.org/") + item).First();
                    list.Add(data);
                }

            }

            return list;

        }

        public static T dataTableToObject<T>(DataTable dt)
        {
            var json = JsonConvert.SerializeObject(dt);
            T myobject = JsonConvert.DeserializeObject<T>(json);

            return myobject;
        }


        //reserved notused
        private DataTable objToDataTable(dynamic obj)
        {
            DataTable dt = new DataTable();

            return dt;
        }

    }



}