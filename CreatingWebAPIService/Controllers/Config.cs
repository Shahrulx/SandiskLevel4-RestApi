﻿using RestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestApi.Controllers
{
    public class Config
    {
        private static volatile Config instance;
        private static object syncRoot = new Object();

        public List<LicenceModel> licenceModelAll = new List<LicenceModel>();
        public List<LicenceModel> licenceModelTemp = new List<LicenceModel>();
        public int lockKey { get; set; } = 1003;

        public static Config Cfg
        {

            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Config();
                    }
                }

                return instance;
            }
        }
    }

}