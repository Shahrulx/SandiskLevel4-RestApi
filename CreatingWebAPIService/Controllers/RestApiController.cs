﻿using Newtonsoft.Json;
using RestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Configuration;
using System.Web.Http;
using System.Xml.Linq;
using static RestApi.Models.AssignDownTimeModel;

namespace RestApi.Controllers
{
    /// <summary>
    /// REST API KNS
    /// </summary>
    public class RestApiController : ApiController
    {
        static string webservice = WebConfigurationManager.AppSettings["Webservice"];

        //kns update
        [HttpPost]
        [Route("Reset")]
        public string Reset()
        {
            String stat = SQLManager.Reset();
            return stat;
        }

        [HttpGet]
        [Route("GetAppName")]
        public HttpResponseMessage GetAppName()
        {
            var Data = SQLManager.GetAppName();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }


        [HttpPost]
        [Route("CheckLicence")]
        public HttpResponseMessage CheckLicence([FromBody] LicenceModel m)
        {
            var Data = SQLManager.CheckLicence(m);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }

        [HttpGet]
        [Route("LicList")]
        public HttpResponseMessage LicList()
        {
            var Data = SQLManager.LicList();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }


        [HttpPost]
        [Route("GetAppUpdate")]
        public HttpResponseMessage GetAppUpdate([FromBody] DeviceInfo deviceInfo)
        {
            var Data = SQLManager.GetAppUpdate(deviceInfo);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }

        [HttpGet]
        [Route("GetNewDownMachine/{PicType}")]
        public HttpResponseMessage GetNewDownMachine(string PicType)
        {
            var Data = SQLManager.GetNewDownMachine(PicType);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetEquipments")]
        public HttpResponseMessage GetEquipments()
        {
            string Status = "";

            string DummyPageUrl = $"{webservice}/GetEquipments";
            try
            {
                WebClient client = new WebClient();
                client.DownloadData(DummyPageUrl);
                Status = "SUCCESS";
            }
            catch (Exception ex)
            {
                Status = ex.Message;
            }

            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Status)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }


        [HttpGet]
        [Route("LockTest/{toolname}")]
        public HttpResponseMessage LockTest(string toolname)
        {
            string Status = "";
      
            var value = toolname.Split(',');
            string DummyPageUrl = $"{webservice}/LockEquipment?toolName=" + value[0] + "&username=" + value[1] + "";
       
            try
            {
                WebClient client = new WebClient();
                client.DownloadData(DummyPageUrl);
                Status = "SUCCESS";
            }
            catch (Exception ex)
            {
                Status = ex.Message;
            }

            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Status)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }


        [HttpGet]
        [Route("UnlockTest/{data}")]
        public HttpResponseMessage UnlockTest(string data)
        {
            string Status = "";

            var value = data.Split(',');
            string DummyPageUrl = $"{webservice}/UnlockEquipment?toolName={value[0]}&username={value[1]}&password={value[2]}&comment={value[3]}&solution=true";
          


           
            List<string> datanamea = new List<string>() { "Status", "IsAPCChecklistAvailable", "Comment" };
            var _hitpage1 = HitPage(DummyPageUrl, datanamea);
           try
            {
                UnlockModel unlockModel = new UnlockModel();
                List<string> dataname = new List<string>() { "Status", "IsAPCChecklistAvailable", "Comment" };
                var _hitpage = HitPage(DummyPageUrl, dataname);

                unlockModel.Status = bool.Parse(_hitpage[0].Value);
                unlockModel.IsAPCChecklistAvailable = bool.Parse(_hitpage[1].Value);
                unlockModel.Comment = _hitpage[2].Value;

                var sz = JsonConvert.SerializeObject(unlockModel);
                Status = sz;
            }
            catch (Exception ex)
            {
                Status = ex.Message;
            }

            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Status)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }


        [HttpPost]
        [Route("UnlockMachine")]
        public HttpResponseMessage UnlockMachine([FromBody] UnlockPostModel p)
        {
            var Data = SQLManager.UnlockMachine(p);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }

        [HttpPost]
        [Route("CloseTicket")]
        public HttpResponseMessage CloseTicket([FromBody] UnlockPostModel p)
        {
            var Data = SQLManager.CloseTicket(p);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }
        
        [HttpPost]
        [Route("CloseMachine")]
        public HttpResponseMessage CloseMachine([FromBody] UnlockPostModel p)
        {
            var Data = SQLManager.CloseMachine(p);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }


        [HttpGet]
        [Route("ServiceTest")]
        public HttpResponseMessage ServiceTest()
        {
            var Data = SQLManager.ServiceTest();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }

        [HttpGet]
        [Route("TestRest")]
        public HttpResponseMessage TestRest()
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("Success")
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }

        [HttpPost]
        [Route("TestPost")]
        public HttpResponseMessage TestPost([FromBody] ModelTest m)
        {
            var Data = m.id;
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }


        [HttpGet]
        [Route("TestConnection")]
        public HttpResponseMessage TestConnection()
        {
            var Data = SQLManager.TestConnection();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }

        [HttpPost]
        [Route("UpdateBattery")]
        public HttpResponseMessage UpdateBattery([FromBody] DeviceInfo deviceInfo)
        {
            var Data = SQLManager.UpdateBattery(deviceInfo);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }



        [HttpPost]
        [Route("GetUser")]
        public HttpResponseMessage GetUser([FromBody] UserInfo userInfo)
        {
            var Data = SQLManager.GetUser(userInfo.Username, userInfo.Password);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }


        [HttpPost]
        [Route("UpdateBarcode")]
        public HttpResponseMessage UpdateBarcode([FromBody] BarcodeData barcodeData)
        {
            var Data = SQLManager.UpdateBarcode(barcodeData);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }


        [HttpGet]
        [Route("GetLine")]
        public HttpResponseMessage GetLine()
        {
            var Data = SQLManager.GetLine();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetAllLine")]
        public HttpResponseMessage GetAllLine()
        {
            var Data = SQLManager.GetAllLine();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetMachineByLineName/{LineName}")]
        public HttpResponseMessage GetMachineByLineName(string LineName)
        {
            var Data = SQLManager.GetMachineByLineName(LineName);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetDownTimeDescription")]
        public HttpResponseMessage GetDownTimeDescription()
        {
            var Data = SQLManager.GetDownTimeDescription();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpPost]
        [Route("SubmitTicket")]
        public HttpResponseMessage SubmitTicket([FromBody] ModelSubmitTicket m)
        {
            var Data = SQLManager.SubmitTicket(m);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }



        [HttpGet]
        [Route("CountLockedMachine")]
        public HttpResponseMessage CountLockedMachine()
        {
            var Data = SQLManager.CountLockedMachine();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("CountLockedMachine/{PIC}")]
        public HttpResponseMessage CountLockedMachine(string PIC)
        {
            var Data = SQLManager.CountLockedMachine(PIC);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetDownMachineByLine/{str}")]
        public HttpResponseMessage GetDownMachineByLine(string str)
        {
     
            var Data = SQLManager.GetDownMachineByLine(str);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }


        [HttpGet]
        [Route("UpdateBeacon/{Position}")]
        public HttpResponseMessage UpdateBeacon(string Position)
        {
            var Data = SQLManager.UpdateBeacon(Position);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        

        [HttpPost]
        [Route("ACKDownTime")]
        public HttpResponseMessage ACKDownTime([FromBody] ACKDownTime p)
        {
            var Data = SQLManager.ACKDownTime(p);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }

        [HttpGet]
        [Route("ACKDownTimeCheckCanAttend/{Id}")]
        public HttpResponseMessage ACKDownTimeCheckCanAttend(string Id)
        {
            var Data = SQLManager.ACKDownTimeCheckCanAttend(Id);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }

        [HttpPost]
        [Route("PassOver")]
        public HttpResponseMessage PassOver([FromBody] PassoverDownTime p)
        {
            var Data = SQLManager.PassOver(p);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }

        private List<XElement> HitPage(string DummyPageUrl, List<string> dataname, bool NoChild = false)
        {
            List<XElement> list = new List<XElement>();

            XDocument doc = XDocument.Load(DummyPageUrl);
            foreach (var item in dataname)
            {

                if (NoChild)
                {
                    XElement x = XElement.Parse(doc.ToString());
                    var val = x.Value.ToString();
                    XElement x2 = XElement.Parse($"<Root><Child>{val}</Child></Root>");
                    list.Add(x2);
                }
                else
                {
                    var data = doc.Descendants(XNamespace.Get("http://tempuri.org/") + item).First();

                    list.Add(data);
                }

            }


            return list;

        }


    }
}
