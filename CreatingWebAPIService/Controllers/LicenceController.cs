﻿using Newtonsoft.Json;
using RestApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace RestApi.Controllers
{
    public class LicenceController
    {

        internal string UpdateLocalLicence(LicenceModel m, string path)
        {
            var _cryp = GetFromFile(path);
            Config.Cfg.licenceModelTemp = JsonConvert.DeserializeObject<List<LicenceModel>>(_cryp);
            var idx = Config.Cfg.licenceModelTemp.FindIndex(x => x.LicKey == m.LicKey);
            if (idx == -1)
            {
                Config.Cfg.licenceModelTemp.Add(m);
            }
            else
            {
                Config.Cfg.licenceModelTemp[idx].LicStatus = 1;
                Config.Cfg.licenceModelTemp[idx].DeviceSerial = m.DeviceSerial;
                return JsonConvert.SerializeObject(Config.Cfg.licenceModelTemp[idx]);
            }
            var json = JsonConvert.SerializeObject(Config.Cfg.licenceModelTemp[Config.Cfg.licenceModelTemp.Count-1]);
            SaveLic(path);
            return json;
        }

        internal int GetLocalLicence(LicenceModel m, string path)
        {
            var _cryp = GetFromFile(path);
            Config.Cfg.licenceModelTemp = JsonConvert.DeserializeObject<List<LicenceModel>>(_cryp);
            var idx = Config.Cfg.licenceModelTemp.FindIndex(x => (x.LicKey == m.LicKey) && (x.AppName==m.AppName));
            if (idx == -1)
            {
                //serial not found in local file
                return -1;
            }
            else //serial found in local file
            {
                //no device link to this serial,
                if(Config.Cfg.licenceModelTemp[idx].DeviceSerial == "-1")
                {
                    //register current devce to this serial
                    Config.Cfg.licenceModelTemp[idx].DeviceSerial = m.DeviceSerial;
                    Config.Cfg.licenceModelTemp[idx].LicStatus = 1;
                    SaveLic(path);
                    return 1;
                }
                else if (Config.Cfg.licenceModelTemp[idx].DeviceSerial == m.DeviceSerial)
                {
                    //current serial is for current device return can login
                    return 1;
                }
                else if (Config.Cfg.licenceModelTemp[idx].DeviceSerial != "-1")
                {
                    //serial already taken by other device
                    return 0;
                }
          
            }
            return 1;
        }

        internal string GetCurrentDeviceConfig(LicenceModel m, string path)
        {
            var _cryp = GetFromFile(path);
            Config.Cfg.licenceModelTemp = JsonConvert.DeserializeObject<List<LicenceModel>>(_cryp);
            var idx = Config.Cfg.licenceModelTemp.FindIndex(x => (x.DeviceSerial == m.DeviceSerial) && (x.AppName == m.AppName));
            string json = JsonConvert.SerializeObject(Config.Cfg.licenceModelTemp[idx]);
            return json;
        }

        internal string FailedJson(LicenceModel m)
        {
            m.LicStatus = -1;
            return JsonConvert.SerializeObject(m);
        }

        internal string CheckLicence(string DeviceName, string Location,string AppName, string path)
        {
            var _cryp = GetFromFile(path);
            Config.Cfg.licenceModelTemp = JsonConvert.DeserializeObject<List<LicenceModel>>(_cryp);
            var idx = Config.Cfg.licenceModelTemp.FindIndex(x => x.DeviceName == DeviceName && x.Location == Location && x.AppName==AppName);
            if (idx == -1)
            {
                //lic not found -1
                LicenceModel m = new LicenceModel();
                m.LicStatus = -1;
                string json1 = JsonConvert.SerializeObject(m);
                return json1;
            }
            else
            {
                if (Config.Cfg.licenceModelTemp[idx].LicStatus == 1)
                {
                    Config.Cfg.licenceModelTemp[idx].LicStatus = 0;
                    //lic taken

                }
                else
                {
                    Config.Cfg.licenceModelTemp[idx].LicStatus = 1;
                    SaveLic(path);
                    //MessageBox.Show("Success");
                }
            }
            string json = JsonConvert.SerializeObject(Config.Cfg.licenceModelTemp[idx]);
            return json;
        }

        internal string GetFromFile(string path)
        {
            string text = File.ReadAllText(path);
            var _cryp = Cryp(text, Config.Cfg.lockKey);
            Config.Cfg.licenceModelTemp = JsonConvert.DeserializeObject<List<LicenceModel>>(_cryp);
            return _cryp;
        }

        internal void SetKey(string DeviceName, string Location, int stat)
        {
            var val = Config.Cfg.licenceModelTemp.FindIndex(x => x.DeviceName == DeviceName && x.Location == Location);
            Config.Cfg.licenceModelTemp[val].LicStatus = stat;
        }


        internal void SaveLic(string Path)
        {
            string json = JsonConvert.SerializeObject(Config.Cfg.licenceModelTemp);
            var _cryp = Cryp(json, Config.Cfg.lockKey);
            try
            {
                File.WriteAllText(Path, _cryp);
            }
            catch (Exception ex)
            {
 
            }

        }

        public string GetRegistredDevice(string path)
        {
            string text = File.ReadAllText(path);
            var _cryp = Cryp(text, Config.Cfg.lockKey);
            List<LicenceModel> temp = JsonConvert.DeserializeObject<List<LicenceModel>>(_cryp);
            var filtred = temp.Where(x => x.Enabled == 1).ToList();
            var temp2 = JsonConvert.DeserializeObject<List<LicenceModelFiltred>>(JsonConvert.SerializeObject(filtred));
            return JsonConvert.SerializeObject(temp2);
        }

        public string Cryp(string jsonText, int lockKey)
        {
            StringBuilder inSb = new StringBuilder(jsonText);

            StringBuilder outSb = new StringBuilder(jsonText.Length);
            char Textch;
            for (int iCount = 0; iCount < jsonText.Length; iCount++)
            {
                Textch = inSb[iCount];
                Textch = (char)(Textch ^ lockKey);
                outSb.Append(Textch);
            }
            return outSb.ToString();
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }

}