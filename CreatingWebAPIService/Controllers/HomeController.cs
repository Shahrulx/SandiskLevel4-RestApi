﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace RestApi.Controllers
{
    public class HomeController : ApiController
    {
        [HttpDelete]
        public string DeleteEmpDetails(string id)
        {
            return "Employee details deleted having Id " + id;

        }
        [HttpPut]
        public string UpdateEmpDetails(string Name)
        {
            return "Employee details Updated with Name " + Name;

        }
    }

}

